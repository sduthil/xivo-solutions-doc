.. XiVO-doc Documentation master file.

****************************
XiVO |version| Documentation
****************************

XiVO is an application suite based on several free existing components
including Asterisk_, and our own developments to provide communication services (IPBX, Unified
Messaging, ...) to businesses.

.. _Asterisk: http://www.asterisk.org/

XiVO is `free software`_. Most of its distinctive components, and XiVO as a whole, are distributed
under the *GPLv3 license*.

.. _free software: http://www.gnu.org/philosophy/free-sw.html

XiVO documentation is also available as a downloadable HTML, EPUB or PDF file.
See the `downloads page <https://readthedocs.org/projects/xivo/downloads/>`_
for a list of available files or use the menu on the lower right.


Table of Contents
=================

.. toctree::
   :maxdepth: 2

   introduction/introduction
   installation/installation
   getting_started/getting_started
   upgrade/upgrade
   cti_client/cti_client
   system/system
   ecosystem/ecosystem
   administration/administration
   contact_center/contact_center
   high_availability/high_availability
   api_sdk/api_sdk
   contributors/contributors
   quality_assurance/quality_assurance
   troubleshooting/troubleshooting
   community/community


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
