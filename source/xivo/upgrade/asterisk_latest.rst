.. _upgrade_asterisk_latest:

##########################
Asterisk upgrade procedure
##########################

Introduction
************

There are three distributions available for each release (since release 2017.03).

For XiVO archive repositories

::

	deb http://mirror.xivo.solutions/archive/ xivo-VERSION-latest main
	deb http://mirror.xivo.solutions/archive/ xivo-VERSION-candidate main
	deb http://mirror.xivo.solutions/archive/ xivo-VERSION-oldstable main

Alike for XiVO production repositories

::

	deb http://mirror.xivo.solutions/debian/ xivo-five main
	deb http://mirror.xivo.solutions/debian/ xivo-five-candidate main
	deb http://mirror.xivo.solutions/debian/ xivo-five-oldstable main

* The distribution **latest** contains the current stable version of *XiVO PBX* and Asterisk.
* The distribution **candidate** contains only Asterisk in higher version than the current.
* The distribution **oldstable** contains only Asterisk in the previous stable version.

Example
-------

+-------------------------------+------------------------+---------+---------------------------------------------------------+
| Repository                    | Distribution           | Section | Content                                                 |
+===============================+========================+=========+=========================================================+
| mirror.xivo.solutions/archive | xivo-2017.03-candidate | main    | latest-built-and-tested-version (e.g. asterisk-13.14.0) |
+-------------------------------+------------------------+---------+---------------------------------------------------------+
| mirror.xivo.solutions/archive | xivo-2017.03-latest    | main    | most-stable-known-version (e.g. asterisk-13.13.1)       |
+-------------------------------+------------------------+---------+---------------------------------------------------------+
| mirror.xivo.solutions/archive | xivo-2017.03-previous  | main    | old-most-stable-known-version (e.g. asterisk-13.10.0)   |
+-------------------------------+------------------------+---------+---------------------------------------------------------+

Upgrade to asterisk candidate
*****************************

Before integrating a new version of asterisk in *XiVO PBX*, we first build it and deliver it in the ``xivo-VERSION-candidate`` distribution of our
repository.

The goal is to make it available for specific cases (e.g. urgent bugfixes) before shipping it as the default version.
This page explains how to upgrade/downgrade to/from this candidate version.

.. warning:: This is a specific procedure. You should know what you are doing.


Upgrade to candidate version
****************************

.. warning:: This will upgrade your asterisk version and trigger a *restart* of asterisk.
             You should know what you are doing.

#. Edit the xivo sources list file :file:`/etc/apt/sources.list.d/xivo-dist.list` and add the ``xivo-VERSION-candidate`` distribution
   (see **last line entry** below):

   .. code-block:: bash
     :emphasize-lines: 4

     # xivo-2017.03-latest
     deb http://mirror.xivo.solutions/archive/ xivo-VERSION-latest main
     # deb-src http://mirror.xivo.solutions/archive/ xivo-VERSION-latest main
     deb http://mirror.xivo.solutions/archive/ xivo-VERSION-candidate main

#. Update the packages list::

	apt-get update

#. Check the proposed versions with ``apt-cache policy asterisk``.
   For example it will give you the following result::

    # apt-cache policy asterisk
    asterisk:
      Installed: 8:13.13.1-1~xivo9+13.13.1+20170105.165406.d2ff9a5
      Candidate: 8:13.13.1-1~xivo9+13.13.1+20170105.165406.d2ff9a5
      Version table:
         8:13.14.0-1~xivo9+13.13.1+20170105.165406.d2ff9a5 0
            100 http://mirror.xivo.solutions/archive/ xivo-2017.03-candidate/main amd64 Packages
     *** 8:13.13.1-1~xivo9+20170321.182632.9ad94c7 0
            500 http://mirror.xivo.solutions/archive/ xivo-2017.03-latest/main amd64 Packages

#. Install the new version (install also ``asterisk-dbg`` package if applicable)::

	apt-get -t xivo-VERSION-candidate install asterisk

#. Restart the services (if you have a *XiVO CC*, you should restart its services too)::

    xivo-service restart

**Note:** The priority will prevent installing asterisk from ``xivo-VERSION-candidate`` whenever running **apt-get upgrade** or **xivo-upgrade**.

Downgrade to oldstable version
******************************

.. warning:: This will downgrade your asterisk version and trigger a *restart* of asterisk.
             You should know what you are doing.


#. Edit the xivo sources list file :file:`/etc/apt/sources.list.d/xivo-dist.list` and add the ``xivo-VERSION-oldstable`` distribution
   (see **last line entry** below):

   .. code-block:: bash
     :emphasize-lines: 4

     # xivo-2017.03-latest
     deb http://mirror.xivo.solutions/archive/ xivo-2017.03-latest main
     # deb-src http://mirror.xivo.solutions/archive/ xivo-2017.03-latest main
     deb http://mirror.xivo.solutions/archive/ xivo-2017.03-oldstable main

#. Update the sources list::

    apt-get update

#. Check the proposed versions with ``apt-cache policy asterisk``.
   For example it will give you the following result::

    # apt-cache policy asterisk
    asterisk:
      Installed: 8:13.13.1-1~xivo9+20170321.130355.9ad94c7
      Candidate: 8:13.13.1-1~xivo9+20170321.130355.9ad94c7
      Version table:
     *** 8:13.13.1-1~xivo9+20170321.130355.9ad94c7 1
          100 http://mirror.xivo.solutions/archive/ xivo-2017.03-latest/main amd64 Packages
         8:13.10.0-1~xivo9+2017.01+master+20170208.085934.3b143b7 0
            1 http://mirror.xivo.solutions/archive/ xivo-2017.03-oldstable/main amd64 Packages

#. And downgrade the version by giving the version in the **oldstable** distribution of the repository.
   With the example below (do the same with ``asterisk-dbg`` if applicable)::

    apt-get install asterisk='8:13.10.0-1~xivo9+2017.01+master+20170208.085934.3b143b7'

#. Restart the services (if you have a *XiVO CC*, you should restart its services too)::

    xivo-service restart

Return to the current version
*****************************

.. warning:: This will return your asterisk version to the current most stable version and trigger a *restart* of asterisk.
             You should know what you are doing.

#. Change the source lists with ``xivo-VERSION-latest``::

    xivo-dist xivo-2017.03-latest

#. Check the proposed versions with ``apt-cache policy asterisk``.
   For example it will give you the following result::

    # apt-cache policy asterisk
    asterisk:
      Installed: 8:13.10.0-1~xivo9+2017.02+master+20170301.144142.3b143b7
      Candidate: 8:13.13.1-1~xivo9+20170321.182632.9ad94c7
      Version table:
         8:13.13.1-1~xivo9+20170321.182632.9ad94c7 0
            500 http://mirror.xivo.solutions/archive/ xivo-2017.03-latest/main amd64 Packages
     *** 8:13.10.0-1~xivo9+2017.02+master+20170301.144142.3b143b7 0
            100 /var/lib/dpkg/status

#. And install the version.
   With the example below (do the same with ``asterisk-dbg`` if applicable)::

    apt-get install asterisk

#. Restart the services (if you have a *XiVO CC*, you should restart its services too)::

    xivo-service restart
