.. _recording:

*********
Recording
*********

*XiVO CC* includes a call recording feature: recording is done on *XiVO PBX* and recorded files are sent to the *XiVO CC* Recording server. Then it's possible to search for recordings and download recorded files from *Recording server*.


Recording
=========

For configuring this feature, see :ref:`recording_configuration`.

Description
-----------

Recording feature allows to keep the conversation between caller and callee and stores them on dedicated recording server. All the files are then available for download only for predefined *granted* users.
Recording is done on *XiVO PBX* and sent to *Recording server*. If recorded file can't be synchronized on *XiVO CC* Recording Server, files might be found on *XiVO PBX* in

.. code-block:: bash

    ls -al /var/spool/xivocc-recording/failed

These files will be automatically resynchronized from *XiVO PBX* to *XiVO CC* Recording server each night.

You can then search:

#. by caller, callee, agent number etc.
#. or by callid
#. and then download the recording

.. figure:: search.png
   :scale: 75%

On *Recording server*, one can monitor the space used by the audio files stored in ``Contrôle d'enregistrement`` menu.

.. figure:: store.png
   :scale: 100%


Recording filtering
===================

For configuring this feature, see :ref:`recording_filtering_configuration`.

Description
-----------
*Recording server* allows to prevent some numbers not to be recorded.

You can deactivate recording either

* for specific called numbers (called Incalls or called Queues or called Users),
* or, on outgoing calls, for calling Users internal numbers

.. note:: This feature is designed to activate recording globally (e.g. for every Queues) and then deactivate it for some Queues
          (e.g. for queue 1001)

To do so, navigate to : http://192.168.0.2:9400/recording_control and in page ``Contrôle d'enregistrement`` you can add or remove the
number to disable recording on this number.

.. figure:: stop-record.png
   :scale: 100%


Limitations
-----------

* Recording can only be disabled for specific Incalls, Queues, Users and External called numbers.
* Recording can't be disabled for one Agent.
* Recording can only be disabled by the object number (to disable recording for one queue it must have a number).


Gateway recording
=================

For configuring this gateway, see :ref:`recording_gw_configuration`.

Description
-----------

This gateway offload the duty of recording file from *XiVO PBX* (as it can be costly due to R/W process) and will synchronize files directly with *Recording server*.
It replaces completely the ``xivo-recording`` package.
