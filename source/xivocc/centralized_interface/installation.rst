.. _gcu_installation:

************
Installation
************

Requirements & Limitations
==========================

The XiVO Centralized Interface (XCI) requires :

- A server with:

  - Debian 8
  - PostgreSQL v9.5 (see `Postgresql Wiki <https://wiki.postgresql.org/wiki/Apt>`_ for installing instructions)
  - postgresql-contrib-9.5 package installed
  - Docker>1.12 and corresponding Docker-Compose
  - git installed
  - sudo installed

- Some XiVOs to manage !

.. warning::

        - If you are making circular inclusions of asterisk context the interface can potentially load users for a while, you should be **very** careful with such deployment.
        - In order to use routed numbers, you must create an Incoming calls interval in the `from-exten` context with a did length equal to the internal number length.
        - SCCP devices are not supported an may trigger error in the Centralized interface. You must remove them on your XiVO before using this application.

Automated installation
======================

An installation script is provided to execute all the installations tasks. To run it, execute the following command :

.. code-block:: bash

	curl https://gitlab.com/xivo-utils/icdu-packaging/raw/master/install-icdu.sh | sudo bash

It will ask you a passphrase for generating an SSH key, you need to leave the passphrase empty!

The configuration files are located in ``/etc/docker``.

Run the application
===================

Optionally, you can set a bash alias for conveniently run XCI :

.. code-block:: bash

	alias dcomp='docker-compose -p icdu -f /etc/docker/compose/icdu.yml'

Then simply :

.. code-block:: bash
	
	dcomp up -d

XCI should now be accessible through http://my-server-ip:9001

Application logs
================

1. General application log is in ``/var/log/interface-centralisee/application.log`` with daily rotation, historic logs
   retained for 5 days.
2. User actions are logged to ``/var/log/interface-centralisee/user_actions.log`` with daily rotation, historic logs
   retained for 366 days.

By default ``user_actions.log`` contains only brief information about which authorize XCI user did what action.
To log with more detail (including data of create and update actions), change in
``/etc/docker/interface-centralisee/logback.xml`` line:

.. code-block:: xml

    <logger name="UserActions"  level="INFO">

into:

.. code-block:: xml

    <logger name="UserActions"  level="DEBUG">
