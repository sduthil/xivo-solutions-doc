.. _xivosolutions_release:

****************************
XiVO Solutions Release Notes
****************************

2017.LTS1 (Five)
================

+----------------------+----------------+
| Component            | latest ver.    |
+======================+================+
| xucmgt               | 2017.03.02     |
+----------------------+----------------+
| xuc                  | 2017.03.02     |
+----------------------+----------------+
| recording-server     | 2017.03.02     |
+----------------------+----------------+
| spagobi              | 2017.03.02     |
+----------------------+----------------+
| config-mgt           | 2017.03.02     |
+----------------------+----------------+
| pack-reporting       | 2017.03.02     |
+----------------------+----------------+
| xivo-full-stats      | 2017.03.02     |
+----------------------+----------------+
| xivo-db-replication  | 2017.03.02     |
+----------------------+----------------+
| pgxivocc             | 1.3            |
+----------------------+----------------+
| elasticsearch        | 1.7.2          |
+----------------------+----------------+
| xivoxc_nginx         | 0.8            |
+----------------------+----------------+
| fingerboard          | 0.6            |
+----------------------+----------------+
| kibana_volume        | 0.1            |
+----------------------+----------------+
| recording-rsync      | 1.0            |
+----------------------+----------------+
| XiVO PBX             | 2017.03.02     |
+----------------------+----------------+

2017.03.03
----------

.. note:: Bugfix release. See :ref:`2017.03.00 section <2017_03_00_release>` for feature list and behavior changes.

Consult the `2017.03.03 Roadmap <https://projects.xivo.solutions/versions/39>`_ for complete list of fixes.

Components updated: **xuc**, **xucmgt**

**XUC Server**

* `#961 <https://projects.xivo.solutions/issues/961>`_ - EventDialing when release Hold in conference room on Polycom and Yealink
* `#960 <https://projects.xivo.solutions/issues/960>`_ - State of Agent when dialing while on pause

**Desktop Assistant / Web Assistant**

* `#957 <https://projects.xivo.solutions/issues/957>`_ - Untranslated error after logout when Xuc down and no aut. re-login

**XiVO PBX**

* `#971 <https://projects.xivo.solutions/issues/971>`_ - Fix adding new voicemail to Closed hours :ref:`schedule <schedules>`.

2017.03.02
----------

.. note:: Bugfix release. See :ref:`2017.03.00 section <2017_03_00_release>` for feature list and behavior changes.

Consult the `2017.03.02 Roadmap <https://projects.xivo.solutions/versions/38>`_ for complete list of fixes.

Components updated: **xuc**, **xucmgt**, **XiVO PBX**, **recording-server**, **spagobi**, **config-mgt**, **pack-reporting**, **xivo-full-stats**, **xivo-db-replication**

**CCAgent**

* `#877 <https://projects.xivo.solutions/issues/877>`_ - Transfer from directory search
* `#802 <https://projects.xivo.solutions/issues/802>`_ - Fix error when calling from directory search
* `#739 <https://projects.xivo.solutions/issues/739>`_ - Fix error when Config mgt is down
* `#217 <https://projects.xivo.solutions/issues/217>`_ - Fix first login attempt may fail

**CCManager / Config mgt**

* `#798 <https://projects.xivo.solutions/issues/798>`_ - Secured websocket detection
* `#847 <https://projects.xivo.solutions/issues/847>`_ - Deleted agent group still appears in ConfigMGT & CCManager

**Desktop Assistant / Web Assistant**

* `#931 <https://projects.xivo.solutions/issues/931>`_ - Auto-updater does not quit if close in tray is enabled
* `#844 <https://projects.xivo.solutions/issues/844>`_ - WebRTC - Conversation still ongoing after Directed Pickup
* `#902 <https://projects.xivo.solutions/issues/902>`_ - Null is displayed as forward number if validate an empty string in forward modal
* `#730 <https://projects.xivo.solutions/issues/730>`_ - Should automatically reconnect after error of pc on
* `#588 <https://projects.xivo.solutions/issues/588>`_ - Answering to headset or loudspeaker based on context (Yealink)
* `#558 <https://projects.xivo.solutions/issues/558>`_ - Forwarding can be set to non-digits
* `#786 <https://projects.xivo.solutions/issues/786>`_ - WebRTC: handle error after multiple aborted incomming calls

**Recording**

* `#200 <https://projects.xivo.solutions/issues/200>`_, `#790 <https://projects.xivo.solutions/issues/790>`_ - ``xivo-recording`` and ``call-recording-filtering`` packages were replaced by ``xivocc-recording``. The subroutines shipped in this new package (see: :ref:`recording_configuration`)
  were updated with new ``xivocc-`` prefix to be able to add them on *XiVO PBX* objects (Incalls, Queues ...) via Web interface.

  .. warning:: If these packages were already installed, you **MUST** follow specific :ref:`upgrade_recording_xpbx`.

**Reporting**

* `#775 <https://projects.xivo.solutions/issues/775>`_ - Handle processing error in xivo-full-stats

**XiVO PBX**

* `#846 <https://projects.xivo.solutions/issues/846>`_ - Cannot open an agent group on the second page if there are more than 20 agent groups and the selected group doesn't have more than 20 agents
* `#777 <https://projects.xivo.solutions/issues/777>`_ - Wizard checkbox is set to False after returning from 4th step
* `#746 <https://projects.xivo.solutions/issues/746>`_ - Fix voicemail creation in import
* `#745 <https://projects.xivo.solutions/issues/745>`_ - Handle login casing in import
* `#744 <https://projects.xivo.solutions/issues/744>`_ - Handle login duplicates in import
* `#714 <https://projects.xivo.solutions/issues/714>`_ - Handle special character in SIP registry
* `#463 <https://projects.xivo.solutions/issues/463>`_ - When a user forward (either `*21` or native terminal softkey) to a queue, the callerid is not prepend accordingly to the queue configuration

**XiVO Provisioning**

* `#939 <https://projects.xivo.solutions/issues/939>`_ - Update Polycom firmware 4.0.9 download URL
* `#776 <https://projects.xivo.solutions/issues/776>`_ - Add new MAC address OUI for Polycom (64:16:7F)

**XUC Server**

* `#942 <https://projects.xivo.solutions/issues/942>`_ - Environment variable AUTH_SECRET is not used by xucserver
* `#609 <https://projects.xivo.solutions/issues/609>`_ - Fix SHOW_CALLBACKS environment variable usage

**System**

* Docker images are now labelled with the exact version of the embedded application. See :ref:`admin_version`.

2017.03.01
----------

.. note:: Bugfix release. See :ref:`2017.03.00 section <2017_03_00_release>` for features list and behavior changes.

Consult the `2017.03.01 Roadmap <https://projects.xivo.solutions/versions/37>`_. 

Components updated: **xuc**, **xucmgt**

* Fix LDAP authentication that was no longer working
* Fix auto-update for Desktop Assistant that didn't for Windows
* Fix *Select2Call* key that was not working with secured connection (which includes WebRTC users).
* Update ``xivo-dist`` package to prepare upgrade for future LTS

.. _2017_03_00_release:

2017.03.00
----------

.. note:: **LTS** Release. Below are listed the features, bugfixes and behavior changes.

Consult the `2017.03.00 Roadmap <https://projects.xivo.solutions/versions/13>`_.

**CC Manager**

* CCManager Access management: CCManager access **is now authorized only for users with a with 'Superviseur' or 'Administrateur' rights** (see :ref:`ccmanager-security`).

**Web Assistant / Desktop assistant**

* Name of caller is displayed for incoming calls (when it is known - usually internal users only). When incoming call is
  ringing, name of caller is displayed in popup dialog and in system notification. When call is accepted, name of caller
  is displayed in list of calls.
* Number of missed calls is displayed as number on history button (like number of voice messages on voicemail button).
  Number is cleared once history is displayed.
* Simplification of the forwarding call form by keeping only one field number to configure.
* Added a new header icon to show that you are using **WebRTC** and not a physical phone.
* Added possibility to initiate attended transfer while using **WebRTC** (same UI as for users with physical phones).
* **Desktop Assistant** features only (See :ref:`desktop-assistant` for details):

 * Added **Select2Call** feature with configurable global keyboard shortcut to handle some actions to interact with call.
 * Added `callto:` and `tel:` link support to initiate calls.
 * Added options to close in tray and launch application automatically at startup.

**Xuc Server**

* New Authentication API has been implemented with explicit error handling messages. See :ref:`CTI Authentication <cti_authentication>` for more details.


**XiVO PBX**

* `Asterisk` has been upgraded by default to **13.13.1** version (see `Feature 563
  <https://projects.xivo.solutions/issues/563>`_ for list of fixed bugs).

  .. warning:: This version fixes bug `Calls assigned to busy agents even if ringinuse=0 <https://projects.xivo.solutions/issues/621>`_.
    Then you **SHOULD** take care of removing the palliative ``pre-limit-agentcallback`` subroutine.

* For `asterisk` versions, a new manual procedure is available to switch to oldstable or new candidate one, see :ref:`upgrade_asterisk_latest`.

**System**

* New ``xivocc-dcomp`` alias to manage `XivoCC` docker containers.

  .. warning::
    ``dcomp`` alias was replaced by ``xivocc-dcomp`` script. If you are upgrading from version between 2016.04 and 2017.LTS1 you **must run**::

        unalias dcomp

* Yaml file :file:`/etc/docker/compose/docker-xivocc.yml` now contains all environment variables (like *USE_SSO* etc.).
  Value of these variables are set in two new files:

    * :file:`/etc/docker/compose/factory.env`
    * and :file:`/etc/docker/compose/custom.env`

  To customize XiVO CC edit the ``custom.env`` file.
  The :file:`/etc/docker/compose/.env` file is overwritten every time you run ``xivocc-dcomp`` script with at least one argument.

* To minimize downtime, *XiVO CC* services will not be stopped when upgrading ``xivocc-installer`` package (true for ``xivocc-installer`` package 2017.LTS1 and newer).
* Fix a problem which prevented ``xivocc-installer`` to finish properly if *XiVO PBX* wasn't accessible, even if you had
  choosen not to configure it.
* Upon upgrade, parameter ``ENFORCE_MANAGER_SECURITY`` will be set to ``false`` in file :file:`/etc/docker/compose/custom.env`
  to keep old behavior. If you want to have the new behavior, remove ``ENFORCE_MANAGER_SECURITY`` parameter from the file
  :file:`/etc/docker/compose/custom.env` and recreate the container (do not forget to add *Superviseur* or *Administrateur* rights
  to the users in the Configuration Management server).


2017.02
=======

Consult the `2017.02 Roadmap <https://projects.xivo.solutions/versions/12>`_

+-----------------------------+----------------+
| Component                   | latest ver.    |
+=============================+================+
| XiVO PBX                    | 2017.02        |
+-----------------------------+----------------+
| xivoxc/xivoxc_nginx         | 0.8            |
+-----------------------------+----------------+
| xivoxc/xucmgt               | 2017.02.02     |
+-----------------------------+----------------+
| xivoxc/xuc                  | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/recording-server     | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/spagobi              | 2017.02.latest |
+-----------------------------+----------------+
| xivoxc/config-mgt           | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/pack-reporting       | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/xivo-full-stats      | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/xivo-db-replication  | 2017.02.00     |
+-----------------------------+----------------+
| xivoxc/pgxivocc             | 1.3            |
+-----------------------------+----------------+
| elasticsearch               | 1.7.2          |
+-----------------------------+----------------+
| xivoxc/fingerboard          | 0.6            |
+-----------------------------+----------------+
| xivoxc/kibana_volume        | 0.1            |
+-----------------------------+----------------+
| xivoxc/recording-rsync      | 1.0            |
+-----------------------------+----------------+

**Web Assistant**

* Fix WebRTC implementation compatibility with Chrome v57 and later, see :ref:`webrtc_requirements` for details.
* Fixes and enhancements of error handling while using WebRTC.
* Fixed problem when starting second call while using using SIP phone.

**CCAgent**

* Realtime statistics for inbound calls now take into account **only** ACD calls (it doesn't take anymore internal calls).
* Configuration of :ref:`agent login/pause management using phone function keys <login_pause_funckeys>` was simplified and documented.

**CCManager**

* Added ACD version of Agent statistics of inbound calls. New users have column version of statistics (incl. non-ACD calls)
  hidden by default, but can be added in column selection.
* In Global View when Compact view option is set, agents are displayed on a single line with firstname only and lines without agents are hidden.
* Added new column in Agent View to display if base configuration matches the active configuration (see :ref:`ccmanager_base_configuration_filter`).

**XiVO PBX**

* The subroutines shipped in the package ``xivo-recording`` (see: :ref:`recording_xpbx`) were updated to include the possibility to start recording in pause.
  **Take care to update your installation accordingly.**
* The *Default french configuration* option of the :ref:`Wizard <configuration_wizard>` now configures also 'Default config device' (see :ref:`default_french_conf`).

**Reporting**

* SpagoBI sample reports have been revamped for better consistency and readability. Added profit-sharing + agent activity per week reports.
  To upload these new *sample reports* see :ref:`spagobi`.

  .. note::
    * The new sample reports will be uploaded in a new *Samples* directory.
    * It will not overwrite the old sample report (located under *Standards* and *System* directories).
    * You may want to remove manually the old sample reports.

**System**

* When you add a new user with its line via *XiVO PBX* web interface, it is now visible in *XiVO CC* (CCAgent, CCManager) **without xuc server restart**.
  Limitation: when you are creating a new user, you **must** add the User and its line in the same step.


2017.01
=======

2017.01.01
----------

.. note:: Bugfix release. See :ref:`2017.01.00 section <2017_01_00_release>` for features list and behavior changes.

Consult the `2017.01.01 Roadmap <https://projects.xivo.solutions/versions/34>`_ for the list of fixes shipped in this version.

Bugfix highlight:

* WebAssistant - Cannot login with user account without provisioned device (like DECT or analog device). Temporary solution is to disable WEB RTC feature in xucmgt (see :ref:`webassistant_disable_webrtc`).
* SpagoBI - Fix default url parameter so the container use the host address instead of localhost.

.. _2017_01_00_release:

2017.01.00
----------

Consult the `2017.01 Roadmap <https://projects.xivo.solutions/versions/11>`_

**CCManager**

* CCManager Access management: The CCManager access will be enforced in a near future and will require a user with Supervisor or Administrator rights (defined in the Configuration Management tool).

  .. note::
    * In the meantime, when logging in the CCManager you may get a warning if you user account has no specific right defined
      (Message: "Warning, your user account has no profile defined. Please contact your administrator to create one.").
    * To prevent Agents from accessing the CCManager, you can enforce now the future behavior with a configuration parameter, see :ref:`Enforcing security in CCManager <ccmanager-security>`.

* New hamburger menu is available with option to maximize the different queue views. See :ref:`ccmanager`.
* Queue and Agent views have now fixed table header if size is bigger than single viewport.

**CCAgent**

* Hold/Unhold button is now colorized with proper call on hold status.

**XiVO PBX**

* The :ref:`Wizard <configuration_wizard>` has now a new option *Default french configuration*. This will import a predefined configuration enhanced for french *XiVO PBX* installations (see :ref:`default_french_conf`).
* The asterisk 13.13.1 version is available for upgrade with a specific procedure. See :ref:`upgrade_asterisk_latest`. This version will become the new default asterisk version in a future release but, currently,
  is only available with a specific upgrade procedure.


2016.04
=======

.. warning:: A **Security Issue** related to WebRTC activation in *XiVO PBX* was found in 2016.04.01.

    All *XiVO PBX* installations must be upgraded to latest 2016.04 (see :ref:`XiVO PBX Upgrade notes <upgrade>`).

    See http://mirror.xivo.solutions/security/XIVO-2017-01.pdf


2016.04.01
----------

.. note:: Bugfix release. See :ref:`2016.04.00 section <2016_04_00_release>` for features list and behavior changes.

Consult the `2016.04.01 Roadmap <https://projects.xivo.solutions/versions/25>`_ for the list of fixes shipped in this version.

Bugfix highlight:

* Cannot remotely logout agent from CCManager/Xivo
* Call establishment can take a long time because of the webRTC options


To upgrade, see :ref:`Upgrade to latest subversion <upgrade_latest_subversion_xcc>`.


.. _2016_04_00_release:

2016.04.00
----------

Consult the `2016.04 Roadmap <https://projects.xivo.solutions/versions/8>`_

**System**

* Parameters for :file:`/etc/docker/compose/docker-xivocc.yml` are now stored in :file:`/etc/docker/compose/.env` file. Important
  parameter is ``XIVO_AMI_SECRET``, which holds Ami password.
* To be able to use the :file:`/etc/docker/compose/.env` file, a new ``dcomp`` alias is generated in .bashrc. You must run::

   source .bashrc

  before running ``dcomp`` again.

.. note:: If you are using ``docker-compose`` instead of recommended alias ``dcomp``, make sure your current directory
 is ``/etc/docker/compose``, otherwise ``/etc/docker/compose/.env`` won't be used. i.e.::

  cd /etc/docker/compose
  docker-compose ...

**Web/Desktop Assistant**

* For displaying search result, compatibility with `xivo-dird` of *XiVO PBX* has been enhanced. After upgrade you must
  verify the configuration of your CTI directory Display in *XiVO PBX* as described in :ref:`directories` and :ref:`dird-integration-views`.

.. note:: Integration note: the *Web* and *Desktop Assistant* support only the display of

  * 1 field for name (the one of type *name* in the directory display)
  * 3 numbers (the one of type *number* and the first two of type *callable*)
  * and 1 email

**Callbacks (CCManager)**

* Default csv separator has been changed from pipe '|' to comma ',' for the callback export.


2016.03
=======
No behavior changes.


Archives
========

.. toctree::
   :maxdepth: 2

   old_release_notes
